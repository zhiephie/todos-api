<?php

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// API route group
$router->group(['prefix' => 'api'], function () use ($router) {
    // Matches "/api/register
    $router->post('register', 'AuthController@register');
    // Matches "/api/login
    $router->post('login', 'AuthController@login');
    /*
    * Matches
    * /api/todo (post, get method)
    * /api/todo/id (get, put, delete method)
    */
    $router->post('todos','TodoController@store');
    $router->get('todos', 'TodoController@index');
    $router->get('todos/{id}', 'TodoController@show');
    $router->put('todos/{id}', 'TodoController@update');
    $router->delete('todos/{id}', 'TodoController@destroy');
 });