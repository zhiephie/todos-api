<?php

namespace App\Http\Controllers;

use Illuminate\Http\{Request, JsonResponse};
use App\Models\Todo;

class TodoController extends Controller
{
    protected $todo;

    public function __construct(Todo $todo)
    {
        $this->todo = $todo;
    }

    public function index(): JsonResponse
    {
        $todos = $this->todo->all();

        return response()->json([
            'status' => true,
            'code' => 200,
            'message' => 'List Todo',
            'data' => $todos
        ], 200);
    }

    public function show(int $id): JsonResponse
    {
        $todo = $this->todo->findOrFail($id);

        return response()->json([
            'status' => true,
            'code' => 200,
            'message' => 'Get Todo',
            'data' => $todo
        ], 200);
    }

    public function store(Request $request): JsonResponse
    {
        //validate incoming request 
        $data = $this->validate($request, [
            'name' => 'required|max:100',
            'description' => 'nullable'
        ]);

        try {
            $todo = $this->todo->create($data);
            //return successful response
            return response()->json([
                'status' => true,
                'code' => 201,
                'message' => 'The Data has been created.',
                'data' => $todo
            ], 201);
        } catch (\Exception $e) {
            //return error message
            return response()->json([
                'status' => false,
                'message' => 'Create data todo has been Failed!'
            ], 409);
        }
    }

    public function update(Request $request, int $id): JsonResponse
    {
        //validate incoming request 
        $data = $this->validate($request, [
            'name' => 'required|max:100',
            'description' => 'nullable'
        ]);

        try {
            $todo = $this->todo->findOrFail($id);
            $todo->fill($data);
            $todo->save();
            //return successful response
            return response()->json([
                'status' => true,
                'code' => 200,
                'message' => 'The Data has been updated.',
                'data' => $todo
            ], 200);
        } catch (\Exception $e) {
            //return error message
            return response()->json([
                'status' => false,
                'message' => 'Update data todo has been Failed!'
            ], 409);
        }
    }

    public function destroy(int $id): JsonResponse
    {
        try {
            $todo = $this->todo->findOrFail($id);
            $todo->delete();
            //return successful response
            return response()->json([
                'status' => true,
                'code' => 200,
                'message' => 'The Data has been deleted.'
            ], 200);
        } catch (\Exception $e) {
            //return error message
            return response()->json([
                'status' => false,
                'message' => 'Delete data todo has been Failed!'
            ], 409);
        }
    }
}