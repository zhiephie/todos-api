<?php

namespace App\Http\Controllers;

use Illuminate\Http\{Request, JsonResponse};
use App\Models\User;

class AuthController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function register(Request $request): JsonResponse
    {
        //validate incoming request 
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);

        try {
            $user = $this->user->create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => $request->input('password'),
            ]);

            //return successful response
            return response()->json([
                'status' => true,
                'message' => 'The new User has been created.',
                'user' => $user
            ], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json([
                'status' => false,
                'message' => 'User Registration Failed!'
            ], 409);
        }
    }
}